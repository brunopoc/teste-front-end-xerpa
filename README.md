**Passo a Passo**

Projeto Realizado com React, para inicialo é necessário ter o Node instalado na maquina, caso não tenha, use [esse link](https://nodejs.org/en/download/).

Com o NODE instalado basta instalar as dependencias, use o comando **npm install** para isso.

Após a instalação das dependencias basta usar o comando **npm start**. Aguarde alguns segundos, e caso o seu navegado não abra, basta visitar o link [http://localhost:3000](http://localhost:3000)

Qualquer problema fique avontade em mandar um email para bruno.poc@gmail.com.